
from game import Player, Deck, Whist


# Setup
p1 = Player(1, 'Max')
p2 = Player(2, 'Sam')
p3 = Player(2, 'Phil')
players = [p1, p2, p3]
drawpile = Deck()
drawpile.gen_standard_deck()
whist = Whist(code='ABCD', state='active', players=players, currentround=0,
              drawpile=drawpile)
whist.drawpile.shuffle()
whist.players[0].tricks = 3
whist.players[1].tricks = 2 
whist.players[2].tricks = 1  
whist.cut_cards_tiebreak()
print(whist.tiebreak)
# whist.next_round()
# whist.deal_cards()




