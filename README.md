# Knockout Whist

## How it works:

 - Flask backend, which operates the websockets, stores data and sessions in Redis, handles the game logic, and serves up the built VueJs static files.  You can run the backend with `python app.py`
 - Vuejs front end single page application.  Used the Vue CLI project structure (e.g. mainly in `src`).  Use `npm run build` to generate the finished static files in `\dist`.
 - The config files are setup for local development on windows, and deployment to Heroku.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
