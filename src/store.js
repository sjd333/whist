import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { ToastProgrammatic as Toast } from 'buefy'
export default new Vuex.Store({
  state: {
    count: 0,
    // SOCKETS
    conn: {
        connected: false,
        error: '',
        message: '',
    },
    code: '',
    trumps: '',
    currentround: '',
    players : [],
    outplayers : [],
    hand : [],
    wontrick : '',
    dealer : '',
    gamestate: 'welcome',
    selectedcard: 100, // Holds the ID of the selected card.
    message: 'Get started',
    admin: '',
    active: true,
    user: {
        username: '',
    },
    suitindex: ["\u2665", "\u2663", "\u2660", "\u2666"],  // "\u2660"  ['H','C','S','D']
    numberindex : [2,3,4,5,6,7,8,9,10,"J","Q","K","A"],
  },
  getters: {
    doubleup(state) {
      return state.count * 2;
    },
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    set_counter(state, newCount) {
      state.count = newCount;
    },
    
    // User controls
    set_username(state, newUsername) {
      state.user.username = newUsername;
    },
    
    set_gamestate(state, gamestate) {
      state.gamestate = gamestate;
    },

    set_selectedcard(state, cardindex) {
      state.selectedcard = cardindex;
    },

    // SOCKETS
    SOCKET_CONNECT(state) {
      state.conn.connected = true
    },
    SOCKET_RECONNECT(state) {
      state.conn.connected = true;
      console.log('SOCKET_RECONNECT');
    },
    SOCKET_DISCONNECT(state) {
      state.conn.connected = false
    },
    SOCKET_RECONNECT_ASKROOM(state, message) {
      state.conn.connected = true;
      console.log(message);
      this._vm.$socket.emit('refreshGameData', {'code': state.code });
    },

    SOCKET_NEWJOINER(state, message) {
      console.log('Joiner');
      var data = JSON.parse(message)
      console.log(data)
      state.code = data.code;
      state.gamestate = data.gamestate;
      state.trumps = data.trumps;
      state.currentround = data.currentround;
      state.players = data.players;
      state.outplayers = data.outplayers;
      state.hand = data.hand;
      state.current_turn = data.current_turn;
      state.message = data.message;
      state.active = data.active;
      state.wontrick = data.wontrick;
      state.dealer = data.dealer;
      state.calling_trumps = data.calling_trumps;
      state.tiebreak = data.tiebreak;
      state.admin = data.admin // this is only set at the start.
    },
    SOCKET_GAMEUPDATE(state, message) {
      console.log('Game update');
      var data = JSON.parse(message);
      console.log(data);
      state.code = data.code;
      state.gamestate = data.gamestate;
      state.trumps = data.trumps;
      state.currentround = data.currentround;
      state.players = data.players;
      state.outplayers = data.outplayers;
      Vue.set(state, 'hand', data.hand);
      state.message = data.message;
      state.active = data.active;
      state.wontrick = data.wontrick;
      state.dealer = data.dealer;
      state.calling_trumps = data.calling_trumps;
      state.tiebreak = data.tiebreak;
      state.admin = data.admin;
    },
    
    SOCKET_JOINFAILED(state, message) {
      console.log('Join failed, code below');
      var failedcode = JSON.parse(message).code;
      console.log(failedcode);
      Toast.open({ message: "Code not recognised: " + failedcode, type: "is-danger"});
    },    
    SOCKET_TOASTMESSAGE(state, message) {
      var toastmessage = JSON.parse(message).message;
      var toaststyle = JSON.parse(message).style;
      console.log(toastmessage);
      Toast.open({ message: toastmessage, type: toaststyle});
    },   
    SOCKET_MESSAGE(state, message) {
      console.log(message);
      state.conn.message = JSON.parse(message).data
    },

    SOCKET_ROOM_CREATED(state, message) {
      console.log(message);
      var data = JSON.parse(message)
      state.code = data.code;
    },

    SOCKET_HELLO_WORLD(state, message) {
      state.conn.message = message
    },
    SOCKET_ERROR(state, message) {
      state.conn.error = message.error
    },

  },
  actions: {
    reset_counter(context) {
      context.commit('set_counter', 0)
    }

  } // End actions


});
