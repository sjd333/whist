import VueSocketio from 'vue-socket.io';
import Vuex from 'vuex'
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Buefy from 'buefy'

Vue.use(Buefy)

Vue.config.productionTip = false
require("./assets/main.scss");
Vue.use(Vuex);

var socketaddress = ''
console.log(process.env.NODE_ENV)
if ( process.env.NODE_ENV == 'development' ) {
  socketaddress = `//${window.location.hostname}:5000`;
} else {
  socketaddress = '//';
}
console.log(socketaddress)

Vue.use(VueSocketio, socketaddress, store);
new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
