from flask import Flask, render_template, request, session, redirect, url_for
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask_session import Session
from game import Player, Deck, Whist
from flask_redis import FlaskRedis
import json
import string
import random
import pickle
import eventlet
import os
import requests
import uuid
eventlet.monkey_patch()

app = Flask(__name__, static_url_path='', 
            static_folder='dist',
            template_folder='dist')

platform = os.environ.get('PLATFORM') or "WINDOWS"
if platform == "HEROKU":
    debugmode = False
else: 
    debugmode = True

app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or 'secret!'
app.config['REDIS_URL'] = os.environ.get('REDIS_URL') or "redis://127.0.0.1:6379/0"
app.config['SESSION_TYPE'] = 'redis'

r = FlaskRedis(app)
app.config['SESSION_REDIS'] = r
Session(app)
socketio = SocketIO(app, manage_session=False, cors_allowed_origins="*")

def save_gamedata(code, gameobject):
    pickled_game = pickle.dumps(gameobject)
    r.set(code, pickled_game, ex=7200)


def load_gamedata(code):
    unpacked_object = pickle.loads(r.get(code))
    return unpacked_object

def broadcast_individual_updates(gameobject):
    for player in gameobject.players:
        emit('GAMEUPDATE', json.dumps(gameobject.render_dict(player.id)),
            room=player.id)
    for player in gameobject.outplayers:
        emit('GAMEUPDATE', json.dumps(gameobject.render_dict(player.id)),
            room=player.id)

@app.route('/')
def index():
    if 'id' in session:
        print('Found session: {}'.format(session['id']))
    else:
        session['id'] = uuid.uuid1()
        print('New session id: {}'.format(session['id']))
    return render_template("index.html")

@app.route('/reset')
def reset():
    session.clear()
    return redirect(url_for('index'))


@app.route('/debug/<roomcode>')
def debug(roomcode):
    whist = load_gamedata(roomcode.upper())
    debugdata = whist.render_whist_data()
    playerdebug = []
    
    for player in whist.players:
        current_player = {}
        current_player['inorout'] = 'in'
        current_player['server'] = whist.render_player(player.id)
        current_player['gameupdate'] = whist.render_dict(player.id)
        playerdebug.append(current_player)
    for player in whist.outplayers:
        current_player = {}
        current_player['inorout'] = 'out'
        current_player['server'] = whist.render_player(player.id)
        current_player['gameupdate'] = whist.render_dict(player.id)
        playerdebug.append(current_player)
    
    print(playerdebug)
    return render_template("debug.html", debugdata=debugdata, dubugplayers=playerdebug)
                                                             
@socketio.on('createRoom')
def createroom(message):
    # Creates room and adds the player.
    data = json.loads(message)
    username = data['username']
    userid = data['userid']
    rooms = [x.decode("utf-8") for x in r.keys()]
    print(rooms)
    roomcreated = False
    while roomcreated == False:
        code = ''.join(random.choices(string.ascii_uppercase, k=4))
        if code not in rooms:
            drawpile = Deck()
            drawpile.gen_standard_deck()
            whist = Whist(code=code, state='lobby', currentround=0,
              drawpile=drawpile)
            roomcreated=True
            emit('ROOM_CREATED', json.dumps({'code': code}))
            print('Created room: {}'.format(code))
    whist.add_player(Player(userid, username, admin=True, active=True, sessionid=session['id']))
    whist.activemessage = 'Start when ready'
    whist.inactivemessage = 'Waiting for {} to start game'.format(whist.get_active_playername())
    session['code'] = code
    join_room(code)
    emit('NEWJOINER', json.dumps(whist.render_dict(userid)),
         room=code, broadcast=True)
    save_gamedata(code, whist)

@socketio.on('joinRoom')
def joinroom(message):
    data = json.loads(message)
    code = data['code'].upper()
    username = data['username']
    userid = data['userid']
    rooms = [x.decode("utf-8") for x in r.keys()]
    if code in rooms:
        whist = load_gamedata(code)

        if (len(whist.players) + len(whist.outplayers)) > 7:
            emit('TOASTMESSAGE', json.dumps({'message': 'Too many players', 'style': 'is-danger'}))
            return

        if whist.state != 'lobby':
            emit('TOASTMESSAGE', json.dumps({'message': 'Game in progress', 'style': 'is-warning'}))
            whist.add_player(Player(userid, username, admin=False, active=False, sessionid=session['id']), watching=True)
        else:
            whist.add_player(Player(userid, username, admin=False, active=False, sessionid=session['id']))
        join_room(code)
        save_gamedata(code, whist)
        session['code'] = code
        broadcast_individual_updates(whist)
        
    else:
        print('NO CODE FOUND IN DATABASE')
        print(code)
        print(r.keys())
        emit('JOINFAILED', json.dumps({'code': code}))


@socketio.on('reStartGame')
def restart_game(message):
    data = json.loads(message)
    code = data['code'].upper()
    oldwhist = load_gamedata(code)
    allplayers = []
    for player in oldwhist.players:
        allplayers.append({'name': player.name, 'id': player.id, 'admin': player.admin, 'sessionid': player.sessionid})
    for player in oldwhist.outplayers:
        allplayers.append({'name': player.name, 'id': player.id, 'admin': player.admin, 'sessionid': player.sessionid})
    drawpile = Deck()
    drawpile.gen_standard_deck()
    newwhist = Whist(code=code, state='lobby', currentround=0,
        drawpile=drawpile)       
    for player in allplayers:
        newwhist.add_player(Player(player['id'], player['name'], admin=player['admin'], sessionid=player['sessionid']))
    newwhist.drawpile.shuffle()
    newwhist.next_round()
    newwhist.activemessage = 'Start the hand'
    newwhist.inactivemessage = '{} to play'.format(newwhist.get_active_playername())
    save_gamedata(code, newwhist)
    broadcast_individual_updates(newwhist)

@socketio.on('startGame')
def start_game(message):
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    whist.drawpile.shuffle()
    whist.next_round()
    whist.activemessage = 'Start the hand'
    whist.inactivemessage = '{} to play'.format(whist.get_active_playername())
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)        

@socketio.on('refreshGameData')
def refreshgamedata(message):
    data = json.loads(message)
    code = data['code'].upper()
    playerid = data['id']    
    whist = load_gamedata(code)
    emit('GAMEUPDATE', json.dumps(whist.render_dict(playerid)),
        room=playerid)

@socketio.on('playedCard')
def played_card(message):
    data = json.loads(message)
    print('**************PLAYED**********')
    print(data)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    cardid = data['cardid']
    whist.card_played(userid, cardid)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)

@socketio.on('collectTrick')
def collectTrick(message):
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    whist.next_trick(userid)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)

@socketio.on('dealCards')
def deal_cards(message):
    data = json.loads(message)
    code = data['code'].upper()
    whist = load_gamedata(code)
    userid = data['userid']
    whist.clear_previous_hand()
    save_gamedata(code, whist)
    whist.render_all_dicts()
    broadcast_individual_updates(whist)


@socketio.on('selectedTrumps')
def selectedTrumps(message):
    print(message)
    data = json.loads(message)
    code = data['code'].upper()
    trumps = data['suit']
    whist = load_gamedata(code)
    whist.set_trumps(trumps)
    save_gamedata(code, whist)
    broadcast_individual_updates(whist)


@socketio.on('connect')
def connect():
    # Check if a valid sessioncode exists, removes it if not.
    sessioncode = session.get('code')
    roomfound = False
    if sessioncode is not None:
        rooms = [x.decode("utf-8") for x in r.keys()]
        if sessioncode in rooms:
            roomfound = True
        else:
            session.pop("code", None)
            emit('JOINFAILED', json.dumps({'code': sessioncode}))
    if roomfound:
        whist = load_gamedata(session['code'])
        whist.update_socket_from_session(session['id'], request.sid)
        save_gamedata(session['code'], whist)
        emit('GAMEUPDATE', json.dumps(whist.render_dict(request.sid)),
                room=request.sid)
    else:
        # Normal connection
        emit('my response', {'data': 'Connected'})

@socketio.on('disconnect')
def test_disconnect():
    print('*disconnect*{}'.format(request.sid))

if __name__ == '__main__':
    socketio.run(app, debug=debugmode, host='0.0.0.0')
