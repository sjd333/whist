import random

class Card(object):
    def __init__(self, suit, number, id, value=None, eligible=True, winning=False):
        self.suit = suit
        self.number = number
        self.value = value
        self.eligible = eligible
        self.id = id
        self.winning = winning


class Deck(object):
    def __init__(self, cards=None):
        if cards:
            self.cards = cards
        else:
            self.cards = []

    def shuffle(self):
        random.shuffle(self.cards)

    def remove(self, position='top'):
        if self.count() == 0:
            return None
        if position == 'top':
            pos = 0
        elif position == 'bottom':
            pos = len(self.cards) - 1
        elif position == 'random':
            pos = random.randint(0, len(self.cards) - 1)
        else:
            pos = position
        return self.cards.pop(pos)

    def add(self, card, position='top'):
        if position == 'top':
            self.cards.insert(0, card)
        elif position == 'bottom':
            self.cards.append(card)
        elif position == 'random':
            pos = random.randint(0, len(self.cards) - 1)
            self.cards.insert(pos, card)

    def order(self):
        self.cards.sort(key=lambda x: ((x.suit*13) + x.number), reverse=False)

    def count(self):
        return len(self.cards)

    def get_cardposition_byid(self, cardid):
        cardindex = [card.id for card in self.cards].index(cardid)
        return cardindex

    def __str__(self):
        x = []
        for card in self.cards:
            x.append('S{}N{}'.format(card.suit, card.number))
        return ', '.join(x)

    def gen_standard_deck(self):
        # numbers = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"]
        # suits = ['C', "S", "D", "H"]
        cards = []
        count = 0
        for suit in range(4):
            for number in range(13):
                cards.append(Card(suit, number, count))
                count += 1
        self.cards = cards

class Player(object):
    def __init__(self, id, name, active=False, sessionid=None, connected=True, admin=False, tricks=0,
                 calling_trumps=False, hand=None, playedcard=None, colour=None,
                 start_turn_with=False, wontrick=False, ingame=True, dealer=False):
        self.number = None
        self.id = id
        self.sessionid = sessionid
        self.name = name
        self.active = active
        self.connected = connected
        self.admin = admin
        self.tricks = tricks
        if hand:
            self.hand = hand
        else:
            self.hand = Deck()
        self.colour = colour
        self.calling_trumps = calling_trumps
        self.playedcard = playedcard
        self.wontrick = wontrick
        self.start_turn_with = start_turn_with
        self.ingame = ingame
        self.dealer = dealer

class Whist(object):
    def __init__(self, drawpile, code, state='lobby', currentround=0, players=None, outplayers=None, tiebreak=None):
        self.name = 'Whist'
        self.colourlist = ['Red', 'Blue', 'Yellow', 'Green', 'Purple',
                           'Orange', 'Pink', 'Grey', 'Black']
        self.roundcards = {1: 7, 2: 6, 3: 5, 4: 4, 5: 3, 6: 2, 7: 1}
        self.numbers = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"]
        self.suits = ['C', "S", "D", "H"]
        self.code = code
        self.state = state  # Records the state of the game
        if players:
            self.players = players
        else:
            self.players = []  # List of players
        self.drawpile = drawpile
        self.trumps = None
        self.currentsuit = None
        self.currentround = currentround
        self.activemessage = None
        self.inactivemessage = None
        self.tiebreak = tiebreak
        if outplayers:
            self.outplayers = outplayers
        else:
            self.outplayers = []  # List of players

    def update_socket_from_session(self, sessionid, socketid):
        for player in self.players:
            if player.sessionid == sessionid:
                player.id = socketid
        for player in self.outplayers:
            if player.sessionid == sessionid:
                player.id = socketid

    def out_player(self, playerid):
        playerindex, playerposition = self.get_player_position(playerid)
        outplayer = self.players.pop(playerindex)
        outplayer.active = False
        self.outplayers.append(outplayer)

    def mangage_connected_player(self, playerid, connected=True):
        for player in self.players:
            if player.id == playerid:
                player.connected = connected
        for player in self.outplayers:
            if player.id == playerid:
                player.connected = connected

    def add_player(self, player, watching=False):
        player.colour = self.colourlist.pop(0)
        if watching:
            self.outplayers.append(player)
        else:
            self.players.append(player)

    def add_watchplayer(self, player):
        player.colour = self.colourlist.pop(0)
        self.outplayers.append(player)

    def order_players(self, startingplayerid, settofirstplayer=True):
        startplayerposition = [p.id for p in self.players].index(startingplayerid)
        print(startplayerposition)
        if startplayerposition != 0:
            self.players = self.players[startplayerposition:] + self.players[:startplayerposition]
        if settofirstplayer:
            self.players[0].active = True

    def choose_random_player(self):
        return random.choice(self.players).id

    def set_active_player(self, playerid):
        for player in self.players:
            if player.id == playerid:
                player.active = True
            else:
                player.active = False

    def get_player_position(self, playerid):
        playerindex = [x.id for x in self.players].index(playerid)
        if playerindex == 0:
            return (playerindex, 'first')
        elif playerindex == len(self.players) - 1:
            return (playerindex, 'last')
        else:
            return (playerindex, 'middle')


    def decide_next_trumps(self):
        maxtricks = max([p.tricks for p in self.players])
        topplayers = []
        for player in self.players:
            if player.tricks == maxtricks:
                topplayers.append({'id': player.id, 'name': player.name})
        if len(topplayers)== 1:
            self.set_trump_caller(topplayers[0]['id'])
            self.tiebreak = None
        else:
            self.tiebreak = {}
            self.tiebreak['cutcards'] = []
            valuelist = list(range(13))
            random.shuffle(valuelist)
            counter = 0
            maxvalue = 0
            maxname = None
            maxid = None
            for player in topplayers:
                tb = {}
                tb['id'] = player['id']
                tb['name'] = player['name']
                tb['number'] = valuelist[counter]
                tb['suit'] = random.randrange(4)
                if tb['number'] > maxvalue:
                    maxvalue = tb['number']
                    maxname = tb['name']
                    maxid = tb['id']
                self.tiebreak['cutcards'].append(tb)
                self.tiebreak['winnername'] = maxname
                counter += 1
            self.set_trump_caller(maxid)
        
    def next_round(self):
        self.state = 'game'
        self.currentround += 1
        if self.currentround == 1:
            self.trumps = random.choice([0,1,2,3])
            print('Trumps chosen as {}'.format(self.suits[self.trumps]))
            self.order_players(self.choose_random_player())
            self.set_dealer(self.players[0].id)
            self.deal_cards()
            self.set_active_player(self.players[0].id)
        else:
           self.state = 'gamestatus'
           self.trumps = None
           self.remove_out_players()
           self.next_dealer()
           self.decide_next_trumps()

    def set_trump_caller(self, playerid=None):
        for player in self.players:
            if player.id == playerid:
                player.calling_trumps = True
            else:
                player.calling_trumps = False


    def set_dealer(self, playerid=None):
        for player in self.players:
            if player.id == playerid:
                player.dealer = True
            else:
                player.dealer = False

    def next_dealer(self):
        if True in [x.dealer for x in self.players]:
            dealerindex = [x.dealer for x in self.players].index(True)

            if dealerindex < len(self.players) - 1:
                newdealerindex = dealerindex + 1
            else:
                newdealerindex = 0
            self.set_dealer(self.players[newdealerindex].id)
            print('Dealer is {}'.format(self.players[newdealerindex].name))
        else:
            print('NO DEALER FOUND - POSSIBLY DEALER IS OUT OR GAME OVER?')
            newdealerindex = 0
            self.set_dealer(self.players[newdealerindex].id) # Shoudl really figure out dealer properly.


    def remove_out_players(self):
        outlist = [player.id for player in self.players if player.tricks == 0]
        
        # Give free rides (for first go, and randomly with 33% chance in round 2)
        if (self.currentround - 1) == 1:
            return False
        if (self.currentround -1) == 2:
            grandad = random.randrange(3)
            if grandad == 1:
                return False

        for playerid in outlist:
            self.out_player(playerid)
        if len(self.players)==1:
            winnningplayername = self.players[0].name
            if self.players[0].admin == True:
                self.players[0].active = True
            else:
                self.players[0].active = False
            for player in self.outplayers:
                if player.admin == True:
                    player.active = True
                else:
                    player.active = False
            self.activemessage = '{} won! Restart?'.format(winnningplayername)
            self.inactivemessage = '{} won!'.format(winnningplayername)
            print('END OF THE GAME!!!!!!')            

    def deal_cards(self):
        self.drawpile.shuffle()
        for player in self.players:
            for _ in range(self.roundcards[self.currentround]):
                newcard = self.drawpile.remove()
                player.hand.add(newcard)
                print('Dealt {} to {}'.format(newcard.number, player.name))
            player.hand.order()

    def card_played(self, playerid, cardid):
        # Play the actual card
        playerindex, playerposition = self.get_player_position(playerid)
        cardindex = self.players[playerindex].hand.get_cardposition_byid(cardid)
        targetcard = self.players[playerindex].hand.remove(cardindex)
        self.players[playerindex].playedcard = targetcard
        if playerposition == 'first':
            # Set eligible cards for the round
            self.currentsuit = targetcard.suit
            for player in self.players:
                if player.id == playerid:
                    for card in player.hand.cards:
                        card.eligible = False
                else:
                    cardtofollowsuit = False
                    for card in player.hand.cards:
                        card.eligible = True
                        if card.suit == self.currentsuit:
                            cardtofollowsuit = True
                    if cardtofollowsuit:
                        for card in player.hand.cards:
                            if card.suit != self.currentsuit:
                                card.eligible = False
            self.players[playerindex].active = False
            self.inactivemessage = '{} to play'.format(self.players[playerindex + 1].name)
            self.players[playerindex + 1].active = True
            self.activemessage = 'Your turn'
            
        if playerposition == 'middle':
            self.players[playerindex].active = False
            self.inactivemessage = '{} to play'.format(self.players[playerindex + 1].name)
            self.players[playerindex + 1].active = True
            self.activemessage = 'Your turn'

        self.update_card_values()
        self.update_leading_card()

        if playerposition == 'last':
            for player in self.players:
                for card in player.hand.cards:
                    card.eligible = False
                
                if player.playedcard.winning:
                    player.active = True
                    player.wontrick = True
                    self.inactivemessage = '{} won the trick'.format(player.name)
                else:
                    player.active = False
                    player.wontrick = False
            self.activemessage = 'You won!'


    def next_trick(self, playerid):
        print('HAND:')
        print(self.players[0].hand.cards)
        print(type(self.players[0].hand))
        if not self.players[0].hand.cards:
            print('NEXT ROUND!!')
            for player in self.players:
                if player.wontrick:
                    player.tricks += 1
            self.next_round()
        else:
            for player in self.players:
                player.playedcard = None
                if player.wontrick:
                    player.tricks += 1
                    player.active = True
                    self.inactivemessage = '{} to lead'.format(player.name)
                    for card in player.hand.cards:
                        card.eligible = True
                else:
                    player.active = False
                    for card in player.hand.cards:
                        card.eligible = False
            for player in self.players:
                player.wontrick = False
            self.activemessage = 'Start next trick'
            self.order_players(playerid)

    def set_trumps(self, suitindex):
        self.trumps = suitindex
        for player in self.players:
            player.active = False
        self.players[0].active = True
        self.activemessage = 'Lead the trick'
        self.inactivemessage = '{} to lead'.format(self.players[0].name)
        for card in self.players[0].hand.cards:
            card.eligible = True

    def update_card_values(self):
        for player in self.players:
            if player.playedcard:
                if not player.playedcard.value:
                    if player.playedcard.suit == self.trumps:
                        player.playedcard.value = (player.playedcard.number + 1)+20
                    elif player.playedcard.suit == self.currentsuit:
                        player.playedcard.value = player.playedcard.number + 1
                    else:
                        player.playedcard.value = 0
                        
    def update_leading_card(self):
        currentmax = 0
        for player in self.players:
            if player.playedcard:
                if player.playedcard.value > currentmax:
                    currentmax = player.playedcard.value
        for player in self.players:
            if player.playedcard:
                if player.playedcard.value == currentmax:
                    player.playedcard.winning = True
                else:
                    player.playedcard.winning = False

    def clear_previous_hand(self):
        newdrawpile = Deck()
        newdrawpile.gen_standard_deck()
        newdrawpile.shuffle()
        self.drawpile = newdrawpile
        self.deal_cards()
        self.state = 'game'
        self.currentsuit = None
        self.tiebreak = None
        self.activemessage = 'Call trumps'
        for player in self.players:
            player.playedcard = None
            player.tricks = 0
            player.wontrick = False
            if player.calling_trumps:
                print('PLAYER {} calling trumps'.format(player.name))
                player.active = True                
                self.inactivemessage = '{} to call trumps'.format(player.name)
                for card in player.hand.cards:
                    card.eligible = False
            else:
                player.active = False

            for card in player.hand.cards:
                card.eligible = False

        dealerid = None
        for player in self.players:
            if player.dealer:
                dealerid = player.id
        self.order_players(dealerid, settofirstplayer=False)

    def get_active_playername(self):
        for player in self.players:
            if player.active:
                return player.name

    def render_all_dicts(self):
        for player in self.players:
            print('***** {} *****'.format(player.name))
            print(self.render_dict(player.id))

    def render_dict(self, playerid):
        if playerid in [p.id for p in self.players]:
            playerindex = [p.id for p in self.players].index(playerid)
        else:
            playerindex = None
        response = {}
        response['code'] = self.code
        response['gamestate'] = self.state
        response['trumps'] = self.trumps
        response['currentround'] = self.currentround
        response['tiebreak'] = self.tiebreak
        playerlist = []
        for player in self.players:
            if player.playedcard:
                playedcard = {'suit': player.playedcard.suit,
                              'number': player.playedcard.number,
                              'value': player.playedcard.value,
                              'id':  player.playedcard.id,
                              'winning': player.playedcard.winning}
            else:
                playedcard = None
            playerlist.append({'name': player.name, 'active': player.active,
                              'tricks': player.tricks, 
                              'playedcard': playedcard})
            if player.id == playerid:
                response['calling_trumps'] = player.calling_trumps
                response['wontrick'] = player.wontrick
                response['active'] = player.active
                response['dealer'] = player.dealer
                response['admin'] = player.admin
                if player.active:
                    response['message'] = self.activemessage
                else:
                    response['message'] = self.inactivemessage    
        response['players'] = playerlist

        outplayerlist = []
        for player in self.outplayers:
            outplayerlist.append({'name': player.name, 'id': player.id})
            if player.id == playerid:
                response['calling_trumps'] = False
                response['wontrick'] = False
                response['active'] = False
                response['dealer'] = False
                response['admin'] = player.admin
                response['message'] = self.inactivemessage
        response['outplayers'] = outplayerlist
    
        hand = []
        if playerindex is not None:
            for card in self.players[playerindex].hand.cards:
                hand.append({'suit': card.suit, 'number': card.number, 
                            'eligible': card.eligible, 'id': card.id})
        response['hand'] = hand
        return response

    def render_whist_data(self):
        response = {}
        response['name'] = self.name
        response['code'] = self.code
        response['state'] = self.state
        response['trumps'] = self.trumps
        response['currentsuit'] = self.currentsuit
        response['currentround'] = self.currentround
        response['activemessage'] = self.activemessage
        response['inactivemessage'] = self.inactivemessage
        response['inactivemessage'] = self.inactivemessage
        response['tiebreak'] = self.tiebreak
        return response
        

    def render_player(self, playerid):
        for player in self.players:
            if player.id == playerid:
                response = {}
                response['number'] = player.number
                response['id'] = player.id
                response['sessionid'] = player.sessionid
                response['name'] = player.name
                response['active'] = player.active
                response['connected'] = player.connected
                response['admin'] = player.admin
                response['tricks'] = player.tricks
                response['hand'] = player.hand
                response['colour'] = player.colour
                response['calling_trumps'] = player.calling_trumps
                response['playedcard'] = player.playedcard
                response['wontrick'] = player.wontrick
                response['start_turn_with'] = player.start_turn_with
                response['ingame'] = player.ingame
                response['dealer'] = player.dealer
        for player in self.outplayers:
            if player.id == playerid:
                response = {}
                response['number'] = player.number
                response['id'] = player.id
                response['sessionid'] = player.sessionid
                response['name'] = player.name
                response['active'] = player.active
                response['connected'] = player.connected
                response['admin'] = player.admin
                response['tricks'] = player.tricks
                response['hand'] = player.hand
                response['colour'] = player.colour
                response['calling_trumps'] = player.calling_trumps
                response['playedcard'] = player.playedcard
                response['wontrick'] = player.wontrick
                response['start_turn_with'] = player.start_turn_with
                response['ingame'] = player.ingame
                response['dealer'] = player.dealer
        return response
